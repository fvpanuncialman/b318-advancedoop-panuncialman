class Equipment {
	constructor(equipmentType){
		this.equipmentType = equipmentType
	}
	printInfo(){
		return  `Info: ${this.equipmentType}`;
	}
}


class Bulldozer extends Equipment {
	constructor(equipmentType, model, bladeType){
	super(equipmentType);
	this.model = model; 
	this.bladeType = bladeType;
	}

	printInfo(){
		return super.printInfo() +  `\nThe ${this.equipmentType} ${this.model} has a ${this.bladeType} blade`;
	}
}

let bulldozer1 = new Bulldozer("bulldozer", "Brute", "Shovel");
console.log(bulldozer1.printInfo());


class TowerCrane extends Equipment{
	constructor(equipmentType, model, hookRadius, maxCapacity){
	super(equipmentType)
	this.model = model; 
	this.hookRadius = hookRadius; 
	this.maxCapacity = maxCapacity; 
	}

	printInfo(){
		return super.printInfo() + `\nThe ${this.equipmentType} ${this.model} has ${this.hookRadius} cm hook radius and ${this.maxCapacity} kg max capacity`;
	}
}

let towerCrane1 = new TowerCrane("tower crane", "Pelican", 100, 1500);
console.log(towerCrane1.printInfo());

class Backloader extends Equipment{ 
	constructor(equipmentType, model, type, tippingLoad){
	super(equipmentType)
	this.model = model; 
	this.type = type; 
	this.tippingLoad = tippingLoad; 
	}

	printInfo(){
		return super.printInfo() + `\nThe loader ${this.model} is a hydraulic loader and has a tipping load of ${this.tippingLoad} lbs.`;
	}
}

let backLoader1 = new Backloader ("back loader", "Turtle", "hydraulic", 1500);
console.log(backLoader1.printInfo())

class RegularShape { 
	constructor(noSides, length){
		this.noSides = noSides; 
		this.length = length; 
	}

	getPerimeter(){
		return `The perimeter is ` + this.noSides * this.length
	}

	getArea(){
		return `The area is `
	}
}

class Triangle extends RegularShape{ 
	constructor(noSides, length){
	super(noSides, length)
	}
	
	getArea(){
	const s = (this.length + this.length + this.length) / 2;
    const area = Math.sqrt(s * (s - this.length) * (s - this.length) * (s - this.length)
    );
    return `The area is ${area}`;
  }
}

let triangle1 = new Triangle (3, 10)
console.log(triangle1.getPerimeter()); //The perimeter is 30
console.log(triangle1.getArea()); //The area is 43.30127018922193

class Square extends RegularShape{ 
	constructor(noSides, length){
	super(noSides, length)
	}
	getArea(){ 
		return `The area is ${this.length * this.length}`;
	}

}

let square1 = new Square (4,12)
console.log(square1.getPerimeter()); //The perimeter is 48
console.log(square1.getArea());//The area is 144