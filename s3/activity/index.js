class RegularShape { 
	constructor(noSides, length){
		this.noSides = noSides; 
		this.length = length; 
	
	if(this.constructor === RegularShape){
			throw new Error("Object cannot be created from an abstract class RegularShape")
	}

	if(this.getPerimeter() === undefined){
			throw new Error("Class must implement getPerimeter() method");
	}

	if(this.getArea() === undefined){
			throw new Error("Class must implement getArea() method");
	}
	}
}
class Square extends RegularShape{ 
	constructor(noSides, length){
		super(noSides, length)
	}
	getPerimeter(){
		return `The perimeter of the square is ` + (this.noSides * this.length);
	}

	getArea(){
	return `The area of the square ${this.length * this.length}`;
  	}
}

const shape1 = new Square(4,16)
console.log(shape1.getPerimeter()); //Output: The perimeter of the square is 64
console.log(shape1.getArea()); //Output: The area of the square 256

class Food { 
	constructor(name, price){
		this.name = name; 
		this.price = price; 
	
	if(this.constructor === Food){
			throw new Error("Object cannot be created from an abstract class Food")
	}
	
	if(this.getName() === undefined){
			throw new Error("Class must implement getName() method");
	}
	}
}

class Vegetable extends Food { 
	constructor(name, breed, price){
		super(name, price)

		this.breed = breed;
		}
		getName(){
			return `${this.name} is of ${this.breed} variety and is priced at ${this.price} pesos.`;
		}
}


const vegetable1 = new Vegetable("Pechay", "Native", 25)
console.log(vegetable1.getName()); //Output: Pechay is of Native variety and is priced at 25 pesos.


class Equipment { 
	constructor(equipmentType, model){
		this.equipmentType = equipmentType;
		this.model = model;

	if(this.constructor === Equipment){
			throw new Error("Object cannot be created from an abstract class Equipment")
	}

	if(this.printInfo() === undefined){
			throw new Error("Class must implement printInfo() method");
	}
	}
}

class Bulldozer extends Equipment {
	constructor(equipmentType, model, bladeType){
	super(equipmentType, model)
	this.bladeType = bladeType;
	}
	printInfo(){
		return `Info: ${this.equipmentType} \nThe ${this.equipmentType} ${this.model} has a ${this.bladeType} blade`;
	}
}

let bulldozer1 = new Bulldozer("bulldozer", "Brute", "Shovel");
console.log(bulldozer1.printInfo()); // Output: Info: bulldozer 
				// The bulldozer Brute has a Shovel blade 

class TowerCrane extends Equipment{
	constructor(equipmentType, model, hookRadius, maxCapacity){
	super(equipmentType, model)
	this.hookRadius = hookRadius; 
	this.maxCapacity = maxCapacity; 
	}
	printInfo(){
		return `Info: ${this.equipmentType} \nThe ${this.equipmentType} ${this.model} has ${this.hookRadius} cm hook radius and ${this.maxCapacity} kg max capacity`;
	}
}


let towerCrane1 = new TowerCrane("tower crane", "Pelican", 100, 1500);
console.log(towerCrane1.printInfo()); // Output: Info: tower crane 
				// The tower crane Pelican has 100 cm hook radius and 1500 kg max capacity
	