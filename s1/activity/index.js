class Contractor{
	constructor(name, email, contactNo){
		this.name = name; 
		this.email = email; 
		this.contactNo = contactNo; 
	}

	getContractorDetails(){ 
		return ` Name: ${this.name} \n Email: ${this.email} \n Contact No: ${this.contactNo}`
	}
}

class Subcontractor extends Contractor{
	constructor(name, email, number, specializations){
		super(name, email, number)
		this.specializations = specializations;
	}

	getSubConDetails(){
		return ` Name: ${this.name} \n Email: ${this.email} \n Contact No: ${this.contactNo} \n ${this.specializations}`
	
	}
}

let contractor1 = new Contractor ("Ultra Manpower Services", "ultra@manpower.com", "09167890123");
let subCont1 = new Subcontractor("Ace Bros", "acebros@mail.com", "09151234567", ["gardens", "industrial"]);
let subCont2 = new Subcontractor("LitC corp", "litc@mail.com", "091512456789", ["schools","hospitals", "bakeries", "libraries"]);
console.log(contractor1.getContractorDetails());
console.log(subCont1.getSubConDetails());
console.log(subCont2.getSubConDetails());