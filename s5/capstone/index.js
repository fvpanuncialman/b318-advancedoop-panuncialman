class Request {
	constructor(requesterEmail, content, dateRequested){
		this.requesterEmail = requesterEmail;
		this.content = content; 
		this.dateRequested = dateRequested;
	}

	// getter methods 
	getRequesterEmail(){
		return `Requester Email: ${this.requesterEmail}`;
	}

	getContent(){
		return `Content: ${this.content}`;		
	}
	
	getDateRequested(){
		return `Date Requested: ${this.dateRequested}`;
	}

	// setter methods 

	setRequesterEmail(requesterEmail){
		this.requesterEmail = requesterEmail; 
	} 

	setContent(content){ 
		this.content = content; 
	}

	setDateRequested(dateRequested){
		this.dateRequested = dateRequested;
	}
}

const request1 = new Request();
request1.setRequesterEmail("max@redbull.com");
request1.setContent("Underfloor Package Upgrade");
request1.setDateRequested(new Date());
console.log(request1.getRequesterEmail());
console.log(request1.getContent());
console.log(request1.getDateRequested());
console.log(request1);

class Person { 

	constructor(){

	if(this.constructor === Person){
			throw new Error(
				"Object cannot be created from an abstract class Person"
			);
		}

	if(this.getFullName === undefined){
		throw new Error(
			"Class must implement getFullName() method"
		);
	}
	
	if(this.login === undefined){
			throw new Error("Class must implement login() method");
	}

	if(this.logout === undefined){
			throw new Error("Class must implement logout() method");
	}
	}
}

class Employee extends Person {
	#firstName;
	#lastName;
	#email;
	#department;
	#isActive;
	#requests;

	constructor(firstName, lastName, email, department, isActive, requests){
		super();
		this.#firstName = firstName; 
		this.#lastName = lastName; 
		this.#email = email; 
		this.#department = department; 
		this.#isActive = isActive; 
		this.#requests = []; 
	} 

	// Account access
	login(){
		return `${this.#firstName} ${this.#lastName} has logged in`
	}

	logout(){
		return `${this.#firstName} ${this.#lastName} has logged out`
	}

	// getter methods
	getFullName(){ 
		return `Fullname: ${this.#firstName} ${this.#lastName}`;
	}

	getEmail(){ 
		return this.#email;
	} 

	getDepartment(){
		return `Department: ${this.#department}`;
	}

	getIsActive(){
		return this.#isActive;
	}

	checkRequests(){
		return this.#requests;
	}
	// setter methods 

	setFirstName(firstName){ 
		this.#firstName = firstName;
	}

	setLastName(lastName){
		this.#lastName = lastName; 
	}

	setEmail(email){ 
		this.#email = email;
	} 

	setDepartment(department){
		this.#department = department;
	}

	setIsActive(isActive){
		this.#isActive = isActive;
	}

	addRequest(request) {
   	    this.#requests.push(request);
  	}
}

const employee1 = new Employee();
employee1.setFirstName("Max");
employee1.setLastName("Verstappen");
employee1.setEmail("max@redbull.com");
employee1.setDepartment("Racing Department");
employee1.setIsActive(true);
employee1.addRequest(request1);
console.log(employee1.getFullName());
console.log(employee1.getEmail());
console.log(employee1.login());
console.log(employee1.getDepartment());
console.log(employee1.getIsActive());
console.log(employee1.checkRequests());
console.log(employee1.logout());
console.log(employee1);

class TeamLead extends Person { 
	#firstName;
	#lastName;
	#email;
	#department;
	#isActive;
	#members;

	constructor(firstName, lastName, email, department, isActive, members){
		super();
		this.#firstName = firstName; 
		this.#lastName = lastName; 
		this.#email = email; 
		this.#department = department; 
		this.#isActive = isActive; 
		this.#members = []; 
	}

	// Account access
	login(){
		return `${this.#firstName} ${this.#lastName} has logged in`
	}

	logout(){
		return `${this.#firstName} ${this.#lastName} has logged out`
	}

	// getter methods
	getFullName(){ 
		return `Fullname: ${this.#firstName} ${this.#lastName}`;
	}

	getEmail(){ 
		return `Email: ${this.#email}`;
	} 

	getDepartment(){
		return `Department: ${this.#department}`;
	}

	getIsActive(){
		return this.#isActive;
	}

	getMembers(){
		return this.#members;
	}

	// setter methods 

	setFirstName(firstName){ 
		this.#firstName = firstName;
	}

	setLastName(lastName){
		this.#lastName = lastName; 
	}

	setEmail(email){ 
		this.#email = email;
	} 

	setDepartment(department){
		this.#department = department;
	}

	setIsActive(isActive){
		this.#isActive = true;
	}

	addMember(members){
		this.#members.push(members);
	}
}

const teamlead1 = new TeamLead();
teamlead1.setFirstName("Christian");
teamlead1.setLastName("Hornier");
teamlead1.setEmail("christian@redbull.com");
teamlead1.setDepartment("Red Bull Racing");
teamlead1.setIsActive(true);
teamlead1.addMember(employee1);
console.log(teamlead1.getFullName());
console.log(teamlead1.getEmail());
console.log(teamlead1.login());
console.log(teamlead1.getDepartment());
console.log(teamlead1.getIsActive());
console.log(teamlead1.getMembers());
console.log(teamlead1.logout());
console.log(teamlead1);

class Admin extends Person { 
	#firstName;
	#lastName;
	#email;
	#department;
	#teamLeads;

	constructor(firstName, lastName, email, department, teamLeads){
		super();
		this.#firstName = firstName; 
		this.#lastName = lastName; 
		this.#email = email; 
		this.#department = department; 
		this.#teamLeads = [];
	}

	// Account access
	login(){
		return `${this.#firstName} ${this.#lastName} has logged in`
	}

	logout(){
		return `${this.#firstName} ${this.#lastName} has logged out`
	}

	// getter methods
	getFullName(){ 
		return `Fullname: ${this.#firstName} ${this.#lastName}`;
	}

	getEmail(){ 
		return `Email: ${this.#email}`;
	} 

	getDepartment(){
		return `Department: ${this.#department}`;
	}

	getTeamLeads() {
        return this.#teamLeads;
    }

	// setter methods
	setFirstName(firstName){ 
		this.#firstName = firstName;
	}

	setLastName(lastName){
		this.#lastName = lastName; 
	}

	setEmail(email){ 
		this.#email = email;
	} 

	setDepartment(department){
		this.#department = department;
	}

	addTeamLead(teamLeads){
		this.#teamLeads.push(teamLeads);
	}

	deactivateTeam(teamLeadEmail) {
		for (const teamLead of this.#teamLeads) {
			if (teamLead.getEmail() === teamLeadEmail) {
				teamLead.setIsActive(false);
				const members = teamLead.getMembers();
				for (const member of members) {
					member.setIsActive(false);
				}
				return `Team led by ${teamLead.getFullName()} has been deactivated.`;
			}
		}
		return `Team lead with email ${teamLeadEmail} not found.`;
	}
}

const admin1 = new Admin();
admin1.setFirstName("Helmut");
admin1.setLastName("Marko");
admin1.setEmail("helmut@redbull.com");
admin1.setDepartment("Red Bull Racing");
console.log(admin1.getFullName());
console.log(admin1.getEmail());
console.log(admin1.login());
console.log(admin1.getDepartment());
console.log(admin1.logout());
admin1.addTeamLead(teamlead1);
console.log(admin1.getTeamLeads());
admin1.deactivateTeam("christian@redbull.com");
console.log(teamlead1);